### Template Docker - Laravel

**Configurações**

`PHP FPM: 7.1 Alpine`

`Node: 11.15`

`Nginx: 1.16 Alpine`

`Mysql: 5.7`

`Mailhog: Latest Version`


---

**Configurações Iniciais**

_Primeiro Passo:_

Copie a pasta `docker ` e o arquivo `docker-compose.yml` para a raiz da sua aplicação.


---

_Segundo Passo:_

No arquivo `docker-compose.yml` altere `<app-name>` 
pelo nome da sua aplicação.

---

_Terceiro Passo:_

Abra o arquivo `./docker/nginx/conf.d/default.conf` e na linha que está `server_name: app-name.test`
altere `app-name.test` pela url que deseja usar para acessar a aplicação.

---

_Quarto Passo:_

_Linux/Mac Os_

Abra o arquivo `/etc/hosts` e adicione `127.0.0.1 app-name.test` alterando
 o app-name pelo nome que foi configurado no `/docker/nginx/conf.d/default.conf`.
 
 _Windows_
 
 ...

---

**Comandos**

Para subir os containers:

`docker-compose up -d`

Para derrubar os containers:

`docker-compose stop`

Para entrar no bash do container:

_No Bash você será capaz de rodar comandos dentro do container da aplicação.
Sendo Necessário rodar os comandos no container do php._

`docker exec -it --user 1000:1000 APP_NAME_php_fpm bash`

---

**MySQL**

Dados de Acesso ao MySQL:

_Todas essas configurações são definidas no Enviroment do container, encontrado no `docker-compose.yml`._
    
    MYSQL_DATABASE=<app-name>
    MYSQL_ROOT_PASSWORD=secret
    MYSQL_USER=docker
    MYSQL_PASSWORD=secret